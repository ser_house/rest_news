<?php

namespace App\Tests;

use Psr\Container\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Trait KernelTestCaseTrait
 *
 * Не получится просто взять и отнаследоваться от Symfony\Bundle\FrameworkBundle\Test\KernelTestCase
 * в App\Tests\AppTestListener, поскольку TestCase::addWarning не совместим с TestListener::addWarning.
 * Потому - закат солнца вручную.
 *
 * @package App\Tests
 */
trait KernelTestCaseTrait
{
    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param KernelInterface $kernel
     */
    public function useKernel(KernelInterface $kernel): void
    {
        $this->kernel = $kernel;
        $container = $kernel->getContainer();
        $this->container = $container->has('test.service_container') ? $container->get('test.service_container') : $container;
    }
}
