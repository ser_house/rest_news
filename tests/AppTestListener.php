<?php

namespace App\Tests;

use PHPUnit\Framework\TestListenerDefaultImplementation;
use PHPUnit\Framework\TestSuite;
use PHPUnit\Framework\TestListener;
use App\Kernel;
use PHPUnit\Framework\Test;
use App\Tests\Controller\ApiTest;

/**
 * Class AppTestListener
 *
 * @package App\Tests
 */
class AppTestListener implements TestListener {
	use TestListenerDefaultImplementation;

    use KernelTestCaseTrait;

	/**
	 *
	 */
	private const TEST_SUITE_NAME = 'Project Test Suite';

	/**
	 * @param TestSuite $suite
	 *
	 * @throws \Exception
	 */
	public function startTestSuite(TestSuite $suite): void {
	    $name = $suite->getName();
		if ($this->needKernel($name)) {
            $kernel = new Kernel('test', false);
            $kernel->boot();
            $this->useKernel($kernel);
		}
	}

	/**
	 * @param TestSuite $suite
	 */
	public function endTestSuite(TestSuite $suite): void {
        $name = $suite->getName();
		if ($this->needKernel($name)) {
            $storage_path = $this->container->getParameter('fs.storage_path') . '/' . $this->container->getParameter('fs.storage_name');
            unlink($storage_path);
		}
	}

    /**
     * @param string $suite_name
     *
     * @return bool
     */
	private function needKernel(string $suite_name): bool {
	    return self::TEST_SUITE_NAME === $suite_name
            || ApiTest::class === $suite_name;
    }
}
