<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

/**
 * Class ApiTest
 *
 * @package App\Tests\Controller
 */
class ApiTest extends WebTestCase
{
    /**
     *
     */
    private const URI = '/api/news';

    /** @var array заголовки как они в $_SERVER */
    private $server;

    /** @var array данные новости для создания */
    private $news;

    /** @var string для ручной очистки когда потребуется */
    private $storage_path;


    /**
     *
     */
    protected function setUp()
    {
        parent::setUp();

        $this->server = [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_TOKEN' => '01DR895F58BK4NYY5KYKHRDYMA',
        ];

        $this->news = [
            'title' => ' Повседневная практика показывает',
            'text' => 'По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст. В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит советских времен.',
        ];

        self::bootKernel();

        $container = self::$container;
        $this->storage_path = $container->getParameter('fs.storage_path') . '/' . $container->getParameter('fs.storage_name');
    }

    /**
     *
     */
    public function testCreateSuccessfully()
    {
        $client = static::createClient();

        $this->requestCreate($client, $this->server, $this->news);

        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     *
     */
    public function testMissingToken() {
        $client = static::createClient();

        $server = $this->server;
        unset($server['HTTP_TOKEN']);

        $this->requestCreate($client, $server, $this->news);

        $response = $client->getResponse();

        $this->assertEquals(401, $response->getStatusCode());
    }

    /**
     *
     */
    public function testInvalidData() {
        $client = static::createClient();

        $news = $this->news;
        $news['title'] = '';

        $this->requestCreate($client, $this->server, $news);

        $response = $client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
    }

    /**
     *
     */
    public function testDeleteSuccessfully()
    {
        $client = static::createClient();

        $this->requestCreate($client, $this->server, $this->news);

        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertNotEmpty($content['id']);

        $this->deleteRequest($client, $this->server, $content['id']);

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     *
     */
    public function testNotFound() {
        $client = static::createClient();

        $this->deleteRequest($client, $this->server, '04cf8786-9298-4bbd-a9f0-6c8b7e022308');

        $response = $client->getResponse();

        $this->assertEquals(404, $response->getStatusCode());
    }

    /**
     *
     */
    public function testUpdateSuccessfully() {
        $client = static::createClient();

        $this->requestCreate($client, $this->server, $this->news);

        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertNotEmpty($content['id']);

        $id = $content['id'];

        $news = $this->news;
        $news['title'] = 'New title';
        $news['text'] = 'New text';

        $this->requestUpdate($client, $this->server, $id, $news);
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $this->requestReadOne($client, $this->server, $id);
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertNotEmpty($content['title']);
        $this->assertNotEmpty($content['text']);

        $this->assertEquals($news['title'], $content['title']);
        $this->assertEquals($news['text'], $content['text']);
    }

    /**
     *
     */
    public function testReadAllSuccessfully() {
        unlink($this->storage_path);

        $news1 = $this->news;

        $news2 = $this->news;
        $news2['title'] .= '2';
        $news2['text'] .= '2';

        $news3 = $this->news;
        $news3['title'] .= '3';
        $news3['text'] .= '3';

        $all_news = [
            $news1,
            $news2,
            $news3
        ];

        $client = static::createClient();

        foreach($all_news as $news) {
            $this->requestCreate($client, $this->server, $news);
            $response = $client->getResponse();
            $this->assertEquals(200, $response->getStatusCode());
        }

        $this->requestReadAll($client, $this->server);
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $content = $response->getContent();
        $items = json_decode($content, true);
//        print_r($items); print PHP_EOL;

        $this->assertCount(3, $items);
    }

    /**
     * @param KernelBrowser $client
     * @param array $server
     * @param string $id
     */
    private function deleteRequest(KernelBrowser $client, array $server, string $id) {
        $client->request('DELETE', self::URI . "/$id", [], [], $server);
    }

    /**
     * @param KernelBrowser $client
     * @param array $server
     * @param array $news
     */
    private function requestCreate(KernelBrowser $client, array $server, array $news) {
        $client->request('POST', self::URI, [], [], $server, json_encode($news, JSON_UNESCAPED_UNICODE));
    }

    /**
     * @param KernelBrowser $client
     * @param array $server
     * @param string $id
     * @param array $news
     */
    private function requestUpdate(KernelBrowser $client, array $server, string $id, array $news) {
        $client->request('PUT', self::URI . "/$id", [], [], $server, json_encode($news, JSON_UNESCAPED_UNICODE));
    }

    /**
     * @param KernelBrowser $client
     * @param array $server
     * @param string $id
     */
    private function requestReadOne(KernelBrowser $client, array $server, string $id) {
        $client->request('GET', self::URI . "/$id", [], [], $server);
    }

    /**
     * @param KernelBrowser $client
     * @param array $server
     */
    private function requestReadAll(KernelBrowser $client, array $server) {
        $client->request('GET', self::URI, [], [], $server);
    }
}
