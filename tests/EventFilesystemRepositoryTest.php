<?php

namespace App\Tests;

use App\Core\Event\Created;
use App\Core\Event\Updated;
use App\Core\News;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Core\Event\Deleted;

/**
 * Class EventFilesystemRepositoryTest
 *
 * @package App\Tests
 */
class EventFilesystemRepositoryTest extends KernelTestCase
{
    /** @var \App\Core\IEventRepository */
    private $eventRepository;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();

        $container = self::$container;

        $this->eventRepository = $container->get('App\Core\IEventRepository');
    }

    /**
     * @param string $news_id
     *
     * @throws \Exception
     */
    private function addEvents(string $news_id) {
        $addEvent = new Created($news_id, 'title', 'content');
        $this->eventRepository->add($addEvent);

        // Чтобы получить хоть какую-то разницу по времени.
        sleep(1);

        $titleUpdatedEvent = new Updated($news_id, 'title 2', 'content');
        $this->eventRepository->add($titleUpdatedEvent);

        sleep(1);

        $textUpdatedEvent = new Updated($news_id, 'title 2', 'content 2');
        $this->eventRepository->add($textUpdatedEvent);

        $deletedEvent = new Deleted($news_id, 'title 2', 'content 2');
        $this->eventRepository->add($deletedEvent);
    }

    /**
     * @throws \Exception
     */
    public function testAddEvent()
    {
        $id = Uuid::uuid4();

        $this->addEvents($id);

        $events = $this->eventRepository->findByNewsId($id);
        $this->assertNotEmpty($events);

        print PHP_EOL;

        foreach ($events as $event) {
            print $event->getTime() . ': ' . $event->getName() . PHP_EOL;
        }
    }

    /**
     * @throws \Exception
     */
    public function testApplyEvents()
    {
        $id = Uuid::uuid4();

        $this->addEvents($id);

        $events = $this->eventRepository->findByNewsId($id);
        $this->assertNotEmpty($events);

        $news = new News($id, 'title', 'content');

        $titleUpdatedEvent = new Updated($id, 'title 2', 'content');
        $news->applyEvent($titleUpdatedEvent);

        $expected = new News($id, 'title 2', 'content');

        $this->assertEquals($expected, $news);
    }
}
