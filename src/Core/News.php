<?php

namespace App\Core;

use App\Core\Event\Event;

/**
 * Class News
 *
 * @package App\Core
 */
class News
{

    /** @var string */
    private $id;

    /** @var string */
    private $title;

    /** @var string */
    private $text;

    /**
     * News constructor.
     *
     * @param string $id
     * @param string $title
     * @param string $text
     */
    public function __construct(string $id, string $title, string $text)
    {
        $this->id = $id;
        $this->title = $title;
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function text(): string
    {
        return $this->text;
    }

    /**
     * @param Event $event
     */
    public function applyEvent(Event $event): void
    {
        $this->id = $event->getId();
        $this->title = $event->getTitle();
        $this->text = $event->getText();
    }

    /**
     * @param Event $event
     *
     * @return static
     */
    public static function buildFromEvent(Event $event): self
    {
        return new self($event->getId(), $event->getTitle(), $event->getText());
    }
}
