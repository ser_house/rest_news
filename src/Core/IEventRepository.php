<?php

namespace App\Core;

use App\Core\Event\Event;

/**
 * Interface IEventRepository
 *
 * @package App\Core
 */
interface IEventRepository
{

    /**
     * @param Event $event
     */
    public function add(Event $event): void;

    /**
     * @param string $id
     *
     * @return Event[]
     */
    public function findByNewsId(string $id): array;

    /**
     * @return array
     */
    public function findNewsIds(): array;
}
