<?php

namespace App\Core\Event;

/**
 * Class Event
 *
 * @package App\Core\Event
 */
class Event implements \JsonSerializable
{

    /** @var string */
    protected $id;

    /** @var string */
    protected $title;

    /** @var string */
    protected $text;

    /** @var string */
    protected $time;

    /** @var string */
    protected $name;

    /**
     * Event constructor.
     *
     * @param string $id
     * @param string $title
     * @param string $text
     *
     * @throws \Exception
     */
    public function __construct(string $id, string $title, string $text)
    {
        $this->id = $id;
        $this->title = $title;
        $this->text = $text;
        $this->time = (new \DateTimeImmutable())->format('Y-m-d H:i:s');
        $this->name = get_called_class();
    }

    /**
     * @param \DateTimeImmutable $dateTime
     */
    public function setTime(\DateTimeImmutable $dateTime): void
    {
        $this->time = $dateTime->format('Y-m-d H:i:s');
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getTime(): string
    {
        return $this->time;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return substr(strrchr($this->name, "\\"), 1);
    }

    /**
     * @param string $data
     *
     * @return static
     * @throws \Exception
     */
    public static function jsonDeserialize(string $data): self
    {
        $decoded = json_decode($data, true);
        $event_name = $decoded['name'];
        /** @var self $event */
        $event = new $event_name($decoded['id'], $decoded['title'], $decoded['text']);
        $event->setTime(new \DateTimeImmutable($decoded['time']));
        return $event;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'text' => $this->text,
            'time' => $this->time,
            'name' => $this->name,
        ];
    }
}
