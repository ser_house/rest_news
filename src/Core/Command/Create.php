<?php

namespace App\Core\Command;

/**
 * Class Create
 *
 * @package App\Core\Command
 */
class Create
{
    /** @var string */
    private $id;

    /** @var string */
    private $title;

    /** @var string */
    private $text;

    /**
     * Create constructor.
     *
     * @param string $title
     * @param string $text
     */
    public function __construct(string $title, string $text)
    {
        $this->title = $title;
        $this->text = $text;
    }

    /**
     * @param string $id
     *
     * @return self
     */
    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function text(): string
    {
        return $this->text;
    }
}
