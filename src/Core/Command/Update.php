<?php

namespace App\Core\Command;

/**
 * Class Update
 *
 * @package App\Core\Command
 */
class Update
{
    /** @var string */
    private $id;

    /** @var string */
    private $title;

    /** @var string */
    private $text;

    /**
     * Update constructor.
     *
     * @param string $id
     * @param string $title
     * @param string $text
     */
    public function __construct(string $id, string $title, string $text)
    {
        $this->id = $id;
        $this->title = $title;
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function text(): string
    {
        return $this->text;
    }
}
