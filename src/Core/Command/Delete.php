<?php

namespace App\Core\Command;

/**
 * Class Delete
 *
 * @package App\Core\Command
 */
class Delete
{
    /** @var string */
    private $id;

    /**
     * Delete constructor.
     *
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }
}
