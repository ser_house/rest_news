<?php

namespace App\Core;

/**
 * Interface INewsRepository
 *
 * @package App\Core
 */
interface INewsRepository
{
    /**
     * @param string $id
     *
     * @return News|null
     */
    public function findById(string $id): ?News;

    /**
     * @return array
     */
    public function findAll(): array;
}
