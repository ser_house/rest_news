<?php

namespace App\Core;

/**
 * Class Presenter
 *
 * @package App\Core
 */
class Presenter
{

    /**
     * @param News $news
     *
     * @return View
     */
    public function present(News $news): View
    {
        return new View($news->title(), $news->text());
    }
}
