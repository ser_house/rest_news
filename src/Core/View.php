<?php

namespace App\Core;

class View
{
    /** @var string */
    public $title;

    /** @var string */
    public $text;

    /**
     * View constructor.
     *
     * @param string $title
     * @param string $text
     */
    public function __construct(string $title, string $text)
    {
        $this->title = $title;
        $this->text = $text;
    }
}
