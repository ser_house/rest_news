<?php

namespace App\Core;

/**
 * Class NotFoundException
 *
 * @package App\Core
 */
class NotFoundException extends \Exception
{

}
