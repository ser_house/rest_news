<?php

namespace App\Controller\Api;

use App\Core\Command\Delete as DomainCommand;
use App\Infrastructure\Dispatcher\DeleteCommand;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Infrastructure\Token\ITokenAuthenticatedController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Infrastructure\Validate\IValidateController;

/**
 * Class Delete
 *
 * @package App\Controller\Api
 */
class Delete extends AbstractController implements ITokenAuthenticatedController, IValidateController
{
    /** @var EventDispatcherInterface */
    private $dispatcher;

    /**
     * Delete constructor.
     *
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @inheritDoc
     */
    public function __invoke(string $id)
    {
        $domainCommand = new DomainCommand($id);
        $command = new DeleteCommand($domainCommand);
        $this->dispatcher->dispatch($command, 'news.delete');

        return $this->json(['msg' => 'The news was deleted successfully.', 'id' => $domainCommand->id()]);
    }

}
