<?php

namespace App\Controller\Api;

use App\Core\INewsRepository;
use App\Core\Presenter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ReadOne
 *
 * @package App\Controller\Api
 */
class ReadOne extends AbstractController
{
    /** @var INewsRepository */
    private $newsRepository;

    /** @var Presenter */
    private $presenter;

    /**
     * ReadOne constructor.
     *
     * @param INewsRepository $newsRepository
     * @param Presenter $presenter
     */
    public function __construct(INewsRepository $newsRepository, Presenter $presenter)
    {
        $this->newsRepository = $newsRepository;
        $this->presenter = $presenter;
    }

    /**
     * @inheritDoc
     */
    public function __invoke(string $id)
    {
        $news = $this->newsRepository->findById($id);
        if (null === $news) {
            // Вполне штатная ситуация, никак не исключение.
            return $this->json(['msg' => 'Not found'], 404);
        }

        $view = $this->presenter->present($news);
        $response = JsonResponse::create($view);
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

        return $response;
    }

}
