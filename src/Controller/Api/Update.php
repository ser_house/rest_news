<?php

namespace App\Controller\Api;

use App\Core\Command\Update as DomainUpdateCommand;
use App\Infrastructure\Dispatcher\UpdateCommand;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Infrastructure\Token\ITokenAuthenticatedController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Infrastructure\Validate\IValidateController;
use Symfony\Component\HttpFoundation\Request;
use App\Core\INewsRepository;
use App\Core\NotFoundException;

/**
 * Class Update
 *
 * @package App\Controller\Api
 */
class Update extends AbstractController implements ITokenAuthenticatedController, IValidateController
{
    /** @var EventDispatcherInterface */
    private $dispatcher;

    /** @var INewsRepository */
    private $newsRepository;

    /**
     * Update constructor.
     *
     * @param EventDispatcherInterface $dispatcher
     * @param INewsRepository $newsRepository
     */
    public function __construct(EventDispatcherInterface $dispatcher, INewsRepository $newsRepository)
    {
        $this->dispatcher = $dispatcher;
        $this->newsRepository = $newsRepository;
    }

    /**
     * @param string $id
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws NotFoundException
     */
    public function __invoke(string $id, Request $request)
    {
        $content = $request->getContent();

        $data = json_decode($content, true);

        $news = $this->newsRepository->findById($id);
        if (null === $news) {
            throw new NotFoundException();
        }

        $domainCommand = new DomainUpdateCommand($id, $data['title'], $data['text']);
        $command = new UpdateCommand($domainCommand);
        $this->dispatcher->dispatch($command, 'news.update');

        return $this->json(['msg' => 'Updated.']);
    }

}
