<?php

namespace App\Controller\Api;

use App\Core\INewsRepository;
use App\Core\Presenter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ReadAll
 *
 * @package App\Controller\Api
 */
class ReadAll extends AbstractController
{
    /** @var INewsRepository */
    private $newsRepository;

    /** @var Presenter */
    private $presenter;

    /**
     * ReadAll constructor.
     *
     * @param INewsRepository $newsRepository
     * @param Presenter $presenter
     */
    public function __construct(INewsRepository $newsRepository, Presenter $presenter)
    {
        $this->newsRepository = $newsRepository;
        $this->presenter = $presenter;
    }

    /**
     * @inheritDoc
     */
    public function __invoke(Request $request)
    {
        $news = $this->newsRepository->findAll();
        $views = [];
        foreach ($news as $item) {
            $views[] = $this->presenter->present($item);
        }
        $response = JsonResponse::create($views);
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);
        return $response;
    }

}
