<?php

namespace App\Controller\Api;

use App\Core\Command\Create as DomainCommand;
use App\Infrastructure\Dispatcher\CreateCommand;
use App\Infrastructure\Validate\IValidateController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Infrastructure\Token\ITokenAuthenticatedController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class Create
 *
 * @package App\Controller\Api
 */
class Create extends AbstractController implements ITokenAuthenticatedController, IValidateController
{
    /** @var EventDispatcherInterface */
    private $dispatcher;

    /**
     * Create constructor.
     *
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @inheritDoc
     */
    public function __invoke(Request $request)
    {
        $content = $request->getContent();

        $data = json_decode($content, true);

        $domainCommand = new DomainCommand($data['title'], $data['text']);
        $command = new CreateCommand($domainCommand);
        $this->dispatcher->dispatch($command, 'news.create');

        return $this->json(['msg' => 'The news was added successfully.', 'id' => $domainCommand->id()]);
    }
}
