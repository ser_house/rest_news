<?php

namespace App\Infrastructure;

use App\Core\NotFoundException;
use App\Infrastructure\Token\MissingTokenException;
use App\Infrastructure\Validate\InvalidDataException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

/**
 * Class ExceptionListener
 *
 * @package App\Infrastructure
 */
class ExceptionListener
{
    /** @var string */
    private $env;

    /**
     * ExceptionListener constructor.
     *
     * @param string $env
     */
    public function __construct(string $env)
    {
        $this->env = $env;
    }

    /**
     * @param ExceptionEvent $event
     *
     * @throws \Exception
     */
    public function onKernelException(ExceptionEvent $event)
    {
        $e = $event->getException();

        $response = new JsonResponse();

        if ($e instanceof MissingTokenException) {
            $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
        } elseif ($e instanceof InvalidDataException) {
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
            $response->setData($e->getErrors());
        } elseif ($e instanceof NotFoundException) {
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            $response->setData(['msg' => 'Not found']);
        } else {
            if ('dev' === $this->env) {
                throw $e;
            }

            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $event->setResponse($response);
    }
}
