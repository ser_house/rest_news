<?php

namespace App\Infrastructure\Repository;

use App\Core\INewsRepository;
use App\Core\IEventRepository;
use App\Core\News;

/**
 * Class NewsRepository
 *
 * @package App\Infrastructure\Repository
 */
class NewsRepository implements INewsRepository
{
    /** @var IEventRepository */
    private $eventRepository;

    /**
     * NewsRepository constructor.
     *
     * @param IEventRepository $eventRepository
     */
    public function __construct(IEventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    /**
     * @inheritDoc
     */
    public function findById(string $id): ?News
    {
        $events = $this->eventRepository->findByNewsId($id);
        if (empty($events)) {
            return null;
        }

        $lastEvent = end($events);
        $name = $lastEvent->getName();

        if ('Deleted' === $name) {
            return null;
        }

        $firstEvent = array_shift($events);
        $news = News::buildFromEvent($firstEvent);
        foreach ($events as $event) {
            $news->applyEvent($event);
        }

        return $news;
    }

    /**
     * @inheritDoc
     */
    public function findAll(): array
    {
        $news_ids = $this->eventRepository->findNewsIds();
        if (empty($news_ids)) {
            return null;
        }

        $news_list = [];
        foreach($news_ids as $news_id) {
            $news = $this->findById($news_id);
            // Можем получить и id удаленных, а поскольку новость удалена, то её и нет - пропускаем.
            if (null !== $news) {
                $news_list[] = $news;
            }
        }

        return $news_list;
    }
}
