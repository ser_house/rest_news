<?php

namespace App\Infrastructure\Repository;

use App\Core\Event\Event;
use App\Core\IEventRepository;
use Symfony\Component\Filesystem\Filesystem as SymfonyFilesystem;

/**
 * Class EventFilesystemRepository
 *
 * @package App\Infrastructure\Repository
 */
class EventFilesystemRepository implements IEventRepository
{
    /** @var string */
    private $storage_path;

    /**
     * EventFilesystemRepository constructor.
     *
     * @param string $storage_path
     * @param string $storage_name
     */
    public function __construct(string $storage_path, string $storage_name)
    {
        $fs = new SymfonyFilesystem();

        if (!$fs->exists($storage_path)) {
            $fs->mkdir($storage_path, 0775);
        }

        $this->storage_path = "$storage_path/$storage_name";
    }

    /**
     * @inheritDoc
     */
    public function add(Event $event): void
    {
        file_put_contents($this->storage_path, json_encode($event, JSON_UNESCAPED_UNICODE) . PHP_EOL, FILE_APPEND);
    }

    /**
     * @inheritDoc
     */
    public function findByNewsId(string $id): array
    {
        $content = file_get_contents($this->storage_path);

        $items = array_filter(explode(PHP_EOL, $content));

        $events = [];

        foreach ($items as $item) {
            $event = Event::jsonDeserialize($item);
            if ($id === $event->getId()) {
                $events[] = $event;
            }
        }

        return $events;
    }

    /**
     * @inheritDoc
     */
    public function findNewsIds(): array
    {
        $content = file_get_contents($this->storage_path);

        $items = array_filter(explode(PHP_EOL, $content));

        $ids = [];

        foreach ($items as $item) {
            $event = Event::jsonDeserialize($item);
            $id = $event->getId();
            $ids[$id] = $id;
        }

        return $ids;
    }
}
