<?php

namespace App\Infrastructure\Token;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class TokenSubscriber
 *
 * @package App\Infrastructure\Token
 */
class TokenSubscriber implements EventSubscriberInterface
{
    /**
     * @var
     */
    private $token;

    /**
     * TokenSubscriber constructor.
     *
     * @param $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * @param ControllerEvent $event
     *
     * @throws MissingTokenException
     */
    public function onKernelController(ControllerEvent $event)
    {
        $controller = $event->getController();

        if ($controller instanceof ITokenAuthenticatedController) {
            $token = $event->getRequest()->headers->get('token');
            if ($token !== $this->token) {
                throw new MissingTokenException();
            }
        }
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }
}
