<?php

namespace App\Infrastructure\Token;

use Exception;

/**
 * Class MissingTokenException
 *
 * @package App\Infrastructure\Token
 */
class MissingTokenException extends Exception
{

}
