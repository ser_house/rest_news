<?php

namespace App\Infrastructure\Subscriber;

use App\Infrastructure\Dispatcher\UpdateCommand;
use App\Core\Event\Updated as DomainUpdatedEvent;
use App\Core\IEventRepository;
use App\Infrastructure\Dispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\Core\NotFoundException;
use App\Core\INewsRepository;

/**
 * Class Update
 *
 * @package App\Infrastructure\Subscriber
 */
class Update implements EventSubscriberInterface
{
    /** @var IEventRepository */
    private $eventRepository;

    /** @var INewsRepository */
    private $newsRepository;

    /** @var EventDispatcherInterface */
    private $dispatcher;

    /**
     * Update constructor.
     *
     * @param IEventRepository $eventRepository
     * @param INewsRepository $newsRepository
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        IEventRepository $eventRepository,
        INewsRepository $newsRepository,
        EventDispatcherInterface $dispatcher
    ) {
        $this->eventRepository = $eventRepository;
        $this->newsRepository = $newsRepository;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param UpdateCommand $command
     *
     * @return UpdateCommand
     * @throws NotFoundException
     */
    public function onNewsUpdate(UpdateCommand $command)
    {
        $command->stopPropagation();

        $domainCommand = $command->getDomainCommand();

        $id = $domainCommand->id();

        $news = $this->newsRepository->findById($id);
        if (null === $news) {
            throw new NotFoundException();
        }
        $domainEvent = new DomainUpdatedEvent($id, $domainCommand->title(), $domainCommand->text());

        $this->eventRepository->add($domainEvent);

        $event = new Event($domainEvent);
        $this->dispatcher->dispatch($event, 'news.updated');

        return $command;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            'news.update' => 'onNewsUpdate',
        ];
    }

}
