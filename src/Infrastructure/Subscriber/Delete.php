<?php

namespace App\Infrastructure\Subscriber;

use App\Infrastructure\Dispatcher\DeleteCommand;
use App\Core\Event\Deleted as DomainDeletedEvent;
use App\Core\IEventRepository;
use App\Core\INewsRepository;
use App\Infrastructure\Dispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\Core\NotFoundException;

/**
 * Class Delete
 *
 * @package App\Infrastructure\Subscriber
 */
class Delete implements EventSubscriberInterface
{
    /** @var IEventRepository */
    private $eventRepository;

    /** @var INewsRepository */
    private $newsRepository;

    /** @var EventDispatcherInterface */
    private $dispatcher;

    /**
     * Delete constructor.
     *
     * @param IEventRepository $eventRepository
     * @param INewsRepository $newsRepository
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        IEventRepository $eventRepository,
        INewsRepository $newsRepository,
        EventDispatcherInterface $dispatcher
    ) {
        $this->eventRepository = $eventRepository;
        $this->newsRepository = $newsRepository;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param DeleteCommand $command
     *
     * @return DeleteCommand
     * @throws NotFoundException
     */
    public function onNewsDelete(DeleteCommand $command)
    {
        $command->stopPropagation();

        $domainCommand = $command->getDomainCommand();

        $news = $this->newsRepository->findById($domainCommand->id());
        if (null === $news) {
            throw new NotFoundException();
        }
        $domainEvent = new DomainDeletedEvent($news->id(), $news->title(), $news->text());

        $this->eventRepository->add($domainEvent);

        $event = new Event($domainEvent);
        $this->dispatcher->dispatch($event, 'news.deleted');

        return $command;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            'news.delete' => 'onNewsDelete',
        ];
    }
}
