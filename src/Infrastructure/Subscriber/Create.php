<?php

namespace App\Infrastructure\Subscriber;

use App\Infrastructure\Dispatcher\CreateCommand;
use App\Core\Event\Created as DomainCreatedEvent;
use App\Core\IEventRepository;
use App\Infrastructure\Dispatcher\Event;
use Ramsey\Uuid\Uuid;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class Create
 *
 * @package App\Infrastructure\Subscriber
 */
class Create implements EventSubscriberInterface
{
    /** @var IEventRepository */
    private $repository;

    /** @var EventDispatcherInterface */
    private $dispatcher;

    /**
     * Create constructor.
     *
     * @param IEventRepository $repository
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(IEventRepository $repository, EventDispatcherInterface $dispatcher)
    {
        $this->repository = $repository;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param CreateCommand $command
     *
     * @return CreateCommand
     * @throws \Exception
     */
    public function onNewsCreate(CreateCommand $command)
    {
        $command->stopPropagation();

        $domainCommand = $command->getDomainCommand();

        $id = Uuid::uuid4();

        $domainEvent = new DomainCreatedEvent($id, $domainCommand->title(), $domainCommand->text());

        $this->repository->add($domainEvent);

        $domainCommand->setId($id);

        $event = new Event($domainEvent);
        $this->dispatcher->dispatch($event, 'news.created');

        return $command;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            'news.create' => 'onNewsCreate',
        ];
    }

}
