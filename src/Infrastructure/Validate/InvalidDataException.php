<?php

namespace App\Infrastructure\Validate;

use Exception;

/**
 * Class InvalidDataException
 *
 * @package App\Infrastructure\Validate
 */
class InvalidDataException extends Exception
{
    /** @var array */
    private $errors;

    /**
     * @inheritDoc
     */
    public function __construct(array $errors)
    {
        $this->errors = $errors;
        parent::__construct('Invalid data', 400, null);
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
