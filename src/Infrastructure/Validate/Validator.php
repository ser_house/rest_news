<?php

namespace App\Infrastructure\Validate;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Class Validator
 *
 * @package App\Infrastructure\Validate
 */
class Validator
{

    /**
     * @param string $input_content
     *
     * @return array
     */
    public function validate(string $input_content): array
    {
        $input = json_decode($input_content, true);

        $validator = Validation::createValidator();

        $constraint = new Assert\Collection([
            'title' => [
                new Assert\NotBlank(),
            ],
            'text' => [
                new Assert\NotBlank(),
            ],
        ]);

        $violations = $validator->validate($input, $constraint);
        if (count($violations) > 0) {
            $errors = [];
            /** @var ConstraintViolationInterface $violation */
            foreach ($violations as $violation) {
                $field = $violation->getPropertyPath();
                $msg = $violation->getMessage();
                $errors[] = "$field: $msg";
            }
            return $errors;
        }

        return [];
    }
}
