<?php

namespace App\Infrastructure\Validate;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class ValidateSubscriber
 *
 * @package App\Infrastructure\Validate
 */
class ValidateSubscriber implements EventSubscriberInterface
{
    /** @var Validator */
    private $validator;

    /**
     * ValidateSubscriber constructor.
     *
     * @param Validator $validator
     */
    public function __construct(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param ControllerEvent $event
     *
     * @throws InvalidDataException
     */
    public function onKernelController(ControllerEvent $event)
    {
        $controller = $event->getController();

        if ($controller instanceof IValidateController) {
            $content = $event->getRequest()->getContent();
            $errors = $this->validator->validate($content);
            if ($errors) {
                throw new InvalidDataException($errors);
            }
        }
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }
}
