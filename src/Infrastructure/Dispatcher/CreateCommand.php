<?php

namespace App\Infrastructure\Dispatcher;

use Symfony\Contracts\EventDispatcher\Event as SymfonyEvent;
use App\Core\Command\Create as DomainCommand;

/**
 * Class CreateCommand
 *
 * @package App\Infrastructure\Dispatcher
 */
class CreateCommand extends SymfonyEvent
{
    /** @var DomainCommand */
    private $domainCommand;

    /**
     * CreateCommand constructor.
     *
     * @param DomainCommand $domainCommand
     */
    public function __construct(DomainCommand $domainCommand)
    {
        $this->domainCommand = $domainCommand;
    }

    /**
     * @return DomainCommand
     */
    public function getDomainCommand(): DomainCommand
    {
        return $this->domainCommand;
    }
}
