<?php

namespace App\Infrastructure\Dispatcher;

use Symfony\Contracts\EventDispatcher\Event as SymfonyEvent;
use App\Core\Event\Event as DomainEvent;

/**
 * Class Event
 *
 * @package App\Infrastructure\Dispatcher
 */
class Event extends SymfonyEvent
{
    /** @var DomainEvent */
    private $domainEvent;

    /**
     * Event constructor.
     *
     * @param DomainEvent $domainEvent
     */
    public function __construct(DomainEvent $domainEvent)
    {
        $this->domainEvent = $domainEvent;
    }

    /**
     * @return DomainEvent
     */
    public function getDomainEvent(): DomainEvent
    {
        return $this->domainEvent;
    }
}
