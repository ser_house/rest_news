<?php

namespace App\Infrastructure\Dispatcher;

use Symfony\Contracts\EventDispatcher\Event as SymfonyEvent;
use App\Core\Command\Update as DomainCommand;

/**
 * Class UpdateCommand
 *
 * @package App\Infrastructure\Dispatcher
 */
class UpdateCommand extends SymfonyEvent
{
    /** @var DomainCommand */
    private $domainCommand;

    /**
     * UpdateTitleCommand constructor.
     *
     * @param DomainCommand $domainCommand
     */
    public function __construct(DomainCommand $domainCommand)
    {
        $this->domainCommand = $domainCommand;
    }

    /**
     * @return DomainCommand
     */
    public function getDomainCommand(): DomainCommand
    {
        return $this->domainCommand;
    }
}
