## Задача
написать rest сервис для создания и просмотра новостей используя event sourcing. Работать напрямую с базой запрещено. Запрещено использовать сторонние бандлы (типа jms для сериализации), использовать то, что относится к компонентам самого симфони
__________________________________
## Решение
Для простоты это всего лишь CRUD, без какой-либо бизнес-логики и тем более бизнес-событий (которые и должны быть в ES).
### Процесс такой:
* запрос на API
* контроллер создаёт доменную команду, оборачивает её фреймворковыми событием и отправляет слушателю (одному, поскольку это команда)
* слушатель извлекает доменную команду, создаёт доменное событие (в терминах ES), сохраняет его, создаёт доменное событие типа "произошло" (created, updated и т.д.), оборачивает его фреймворковым событием и отправляет всем возможным получателям

Таким образом получается цепочка Domain Command -> Symfony Event -> Domain Command -> Doman Event -> Symfony Event, чтобы развязать доменные команды и события от фреймворковских.

Хранилище реализовано как файл json.

### Новость
* заголовок
* текст

#### Действия
* создание
    * проверка на токен
* просмотр
    * все
* изменение
    * проверка на токен


### Роуты
#### GET
* api/news
* api/news/{id}

#### PUT
* api/news/{id}

#### POST
* api/news

#### DELETE
* api/news/{id}

